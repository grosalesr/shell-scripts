#!/bin/bash

#
# example: iteration thru declared variables or lines in a file
#

declare -a USR=('zabbix' 'user1' 'user2' 'user3')

for i in "${USR[@]}"; do
	echo $i
done


echo "
Hello World!

Distro
GNU/linux

dot here
" > file.txt

# Reads file by line instead words (separated by space)
while read -r line; do echo "$line"; done < file.txt


