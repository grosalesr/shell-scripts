#!/bin/bash

my_cmd() {
  args=$@
  base_args=" ta da"

VALID_ARGS=$(getopt -o Dsd: --long delete,source:,destination: -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1;
fi

eval set -- "$VALID_ARGS"
while [ : ]; do
  case "$1" in
      -D | --delete)
        echo " removing"
        base_args+=" delete"
        shift
        ;;
      -s | --source)
        echo " source $2"
        src=$2
        shift 2
        ;;
      -d | --destination)
        echo " destination $2"
        target=$2
        shift 2
        ;;
     --) shift;
         break
        ;;
    esac
  done

  echo "cmd $base_args" $src $target
}

main() {
  my_cmd $@
}

main $@
