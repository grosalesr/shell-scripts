Shell Scripts
=============

Misc shell helper scripts, some scripts were removed and its functionality merged to
[minion](https://gitlab.com/grosalesr/minion).


**Current scripts**

* `mdtopdf`: Markdown to PDF conversion using pandoc
* `ls-iommu`: display IOMMU devices for PCI passthrough
* `proxy-ssh`:
    + Do not contaminate the 'known_hosts' file with ephemeral servers connection fingerprints
    + Provides an SSH connection using a given proxy
